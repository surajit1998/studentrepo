<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Stds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stds',function(Blueprint $table){
            $table -> bigIncrements('id');
            $table->string('cne');
            $table->string('fName');
            $table->string('lName');
            $table->integer('age');
            $table->string('speciality');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stds');
    }
}
