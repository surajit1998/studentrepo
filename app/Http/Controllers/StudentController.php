<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Std;

class StudentController extends Controller
{
    public function index()
    {
        $data = Std::all();
        return view('student',['data' => $data, 'layout' => 'index']);
    }

    public function create()
    {
        $data = Std::all();
        return view('student',['data' => $data, 'layout' => 'create']);
    }

    public function store(Request $req)
    {
        $std = new Std;
        $std -> cne = $req -> input('cne');
        $std -> fName = $req -> input('fName');
        $std -> lName = $req -> input('lName');
        $std -> age = $req -> input('age');
        $std -> speciality = $req -> input('speciality');
        $std -> save();
        return redirect('/');
    }

    public function show($id)
    {
        $std = Std::find($id);
        $data = Std::all();
        return view('student',['std'=>$std, 'data' => $data, 'layout' => 'show']);

    }

    public function edit($id)
    {
        $std = Std::find($id);
        $data = Std::all();
        return view('student',['std'=>$std, 'data' => $data, 'layout' => 'edit']);

    }

    public function update(Request $req, $id)
    {
        $std = Std::find($id);
        $std -> cne = $req -> input('cne');
        $std -> fName = $req -> input('fName');
        $std -> lName = $req -> input('lName');
        $std -> age = $req -> input('age');
        $std -> speciality = $req -> input('speciality');
        $std -> save();
        return redirect('/');
    }

    public function destroy($id)
    {
        $std = Std::find($id);
        $std -> delete();
        return redirect('/');
    }



}
