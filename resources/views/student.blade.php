<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <title>Student Management System</title>
  </head>
  <body>
  @include("navBar")
  <div class="row header-container justify-content-center">
    <div class="header">
    <h1>Student Management System</h1></div>
  </div>
    @if($layout == 'index')
    <div class="container-fluid mt-4">
        <div class="row">
            <section class="col-md-8">@include("studentlist")</section>
            <section class="col"></section>
        </div>
    </div>
    @elseif($layout == 'create')
    <div class="container-fluid mt-4">
        <div class="row">
        <section class="col-md-8">@include("studentlist")</section>
            <section class="col">

            <div class="card mb-3">
                <img src="https://etimg.etb2bimg.com/photo/75729614.cms" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Enter the information for new student</h5>
                    <form method="post" action="store">
                    @csrf
                        <div class="form-group">
                            <label>CNE</label>
                            <input type="text" class="form-control" name="cne" placeholder="Enter cne">
                        </div>
                        <div class="form-group">
                            <label>FIRST NAME</label>
                            <input type="text" class="form-control" name="fName" placeholder="Enter first name">
                        </div>
                        <div class="form-group">
                            <label>LAST NAME</label>
                            <input type="text" class="form-control" name="lName" placeholder="Enter last name">
                        </div>
                        <div class="form-group">
                            <label>AGE</label>
                            <input type="number" class="form-control" name="age" placeholder="Enter age">
                        </div>
                        <div class="form-group">
                            <label>SPECIALITY</label>
                            <input type="text" class="form-control" name="speciality" placeholder="Enter speciality">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </form>
                </div>
            </div>
            
            </section>
        </div>
    </div>
    @elseif($layout == 'show')
    <div class="container-fluid">
        <div class="row">
            <section class="col-md-8">@include("studentlist")</section>
            <section class="col"></section>
        </div>
    </div>
    @elseif($layout == 'edit')
    <div class="container-fluid mt-4">
        <div class="row">
            <section class="col-md-8">@include("studentlist")</section>
            <section class="col">
            <div class="card mb-3">
                <img src="https://etimg.etb2bimg.com/photo/75729614.cms" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Edit existing student</h5>
                    <form method="post" action="{{url('/update/'.$std->id)}}">
                    @csrf
                        <div class="form-group">
                            <label>CNE</label>
                            <input type="text" class="form-control" name="cne" value ="{{$std->cne}}" placeholder="Enter cne">
                        </div>
                        <div class="form-group">
                            <label>FIRST NAME</label>
                            <input type="text" class="form-control" name="fName" value ="{{$std->fName}}" placeholder="Enter first name">
                        </div>
                        <div class="form-group">
                            <label>LAST NAME</label>
                            <input type="text" class="form-control" name="lName" value ="{{$std->lName}}" placeholder="Enter last name">
                        </div>
                        <div class="form-group">
                            <label>AGE</label>
                            <input type="number" class="form-control" name="age" value ="{{$std->age}}" placeholder="Enter age">
                        </div>
                        <div class="form-group">
                            <label>SPECIALITY</label>
                            <input type="text" class="form-control" name="speciality" value ="{{$std->speciality}}" placeholder="Enter speciality">
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </form>
                </div>
            </div>
           
            </section>
        </div>
    </div>

    @endif
    
    
  </body>
</html>