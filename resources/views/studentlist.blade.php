<div class="card mb-3">
  <img src="https://images.squarespace-cdn.com/content/v1/59277298e4fcb5e4235e7304/1579299985369-3B6Y9P2AZDWT180RD7I5/ke17ZwdGBToddI8pDm48kG0_wXnvFVYtr8E_VwG1MC17gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0oycmklwMHPwSb2Cr-KYzbquWy9VPGv6wFvhrGUqx6g8NlifStCYa3WZ7XhuX6TLGw/image-asset.jpeg" class="card-img-top" alt="..." height="430px">
  <div class="card-body">
    <h5 class="card-title">List of Students</h5>
    <p class="card-text">You can find here all information about student</p>

    <table class="table table-striped">
  <thead style="background-color: #2FAF88;">
    <tr>
      <th scope="col">CNE</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Age</th>
      <th scope="col">Speciality</th>
      <th scope="col">Operations</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $item)
    <tr>
      <td>{{$item -> cne}}</td>
      <td>{{$item -> fName}}</td>
      <td>{{$item -> lName}}</td>
      <td>{{$item -> age}}</td>
      <td>{{$item -> speciality}}</td>
      <td>
      <a href="#" class="btn btn-sm btn-info">Show</a>
      <a href="{{url('/edit/'.$item -> id)}}" class="btn btn-sm btn-warning">Edit</a>
      <a href="#" class="btn btn-sm btn-danger">Delete</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
  </div>
</div>
